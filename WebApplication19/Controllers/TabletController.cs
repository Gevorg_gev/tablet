using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication19
{
    [ApiController]
    [Route("[controller]")]
    public class TabletController : ControllerBase
    {

        public TabletController(TabletDbCContext tabletDbCContext)
        {
            this.tabletDbCContext = tabletDbCContext; 
        }

        private static readonly string[] Tablet_Name = new[]
        {
            "Samsung Galaxy Tab" , "Apple IPad" , "Xiaomi Mi" , "ASUS"
        };

        private static readonly string[] Tablet_Model = new[]
        {
            "Samsung Galaxy Tab S7 Endge" , "Apple IPad Pro(2021 edition)" , "Xiaomi Mi Tab RS7" , "ASUS Via Tab A6"
        };

        private static readonly string[] Tablet_Color = new[]
        {
            "Dark Blue" , "Gold" , "Matt Black" , "Space Gray" 
        };

        private static readonly string[] Tablet_Country = new[]
        {
            "South Korea" , "United States Of America" , "China" , "Thailand" 
        };
        private TabletDbCContext tabletDbCContext;

        [HttpGet]
        public IEnumerable<Tablet> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Tablet
            {
                Id = rng.Next(1,5),
                Name = Tablet_Name[rng.Next(Tablet_Name.Length)],
                Model = Tablet_Model[rng.Next(Tablet_Model.Length)],
                Color = Tablet_Color[rng.Next(Tablet_Color.Length)],
                Country = Tablet_Country[rng.Next(Tablet_Country.Length)] 
            });
        }

        [HttpPost]
        public IActionResult AddTablet([FromBody] Tablet tablet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            tabletDbCContext.Tablets.Add(tablet);
            tabletDbCContext.SaveChanges(); 

            return Ok();
        } 

        [HttpDelete("{Id}")]
        public IActionResult RemoveTablet([FromRoute] int Id)
        {
            var tab = tabletDbCContext.Tablets.Find(Id);

            if(tab == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                return BadRequest("Wrong Tablet Id\nPlease try again..."); 
            }
            Console.ForegroundColor = ConsoleColor.White;

            tabletDbCContext.Tablets.Remove(tab);
            tabletDbCContext.SaveChanges();

            return Ok(); 
        }

        [HttpPut("{Id}")]
        public IActionResult UpdateTablet([FromRoute] int Id)
        {
            var update = tabletDbCContext.Tablets.Find(Id);
            if(update == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                return BadRequest();
            }
            Console.ForegroundColor = ConsoleColor.White;

            tabletDbCContext.Tablets.Update(update);
            tabletDbCContext.SaveChanges();

            return Ok(); 
        }
    }
}