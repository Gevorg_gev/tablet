namespace WebApplication19
{
    public class Tablet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Country { get; set; } 
    }
}