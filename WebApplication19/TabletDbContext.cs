using Microsoft.EntityFrameworkCore;

namespace WebApplication19
{
    public class TabletDbCContext : DbContext
    {
        public TabletDbCContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Tablet> Tablets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tablet>(x =>
            {
                x.ToTable("Tablets");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}